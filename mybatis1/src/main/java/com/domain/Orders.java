package com.domain;

import java.util.Date;
import java.util.List;

public class Orders {
	// # id, customer_id, shipper_id, employee_id, order_dt
	private String id;	
	private Integer customerId;
	private Integer shipperId;
	private Integer employeeId;
	private Date orderDt;	
	private Customers customer;
	private List<OrderDetails> ordDetails;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
		
	public Customers getCustomer() {
		return customer;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public void setCustomer(Customers customer) {
		this.customer = customer;
	}

	public Integer getShipperId() {
		return shipperId;
	}

	public void setShipperId(Integer shipperId) {
		this.shipperId = shipperId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Date getOrderDt() {
		return orderDt;
	}

	public void setOrderDt(Date orderDt) {
		this.orderDt = orderDt;
	}

	
	public List<OrderDetails> getOrdDetails() {
		return ordDetails;
	}

	public void setOrdDetails(List<OrderDetails> ordDetails) {
		this.ordDetails = ordDetails;
	}

	public String toString() {
		return "Orders [id=" + id + ", customerId=" + customerId
				+ ", shipperId=" + shipperId + ", orderDt=" + orderDt + "]";
	}
}
