package com.domain;

public class OrderDetails {
	// # id, order_id, product_id, quantity
	private Integer id;
	private String orderId;
	private Integer productId;
	private Integer quantity;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String toString() {
		return "OrderDetails [id=" + id + ", orderId=" + orderId
				+ ", productId=" + productId + ", quantity=" + quantity + "]";
	}
}
