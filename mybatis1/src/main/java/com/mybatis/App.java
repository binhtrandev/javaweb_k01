package com.mybatis;

import java.util.List;

import com.domain.Customers;
import com.domain.OrderDetails;
import com.domain.Orders;
import com.domain.User;
import com.mappers.OrdersMapper;
import com.services.OrdersService;
import com.services.UserService;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) {
		// System.out.println( "Hello World!" );
		UserService userService = new UserService();
		/*
		 * User usr = new User(); usr.setEmailId("binhtran@gmail.com");
		 * usr.setPassword("1234"); usr.setFirstName("Tran");
		 * usr.setLastName("Binh");
		 * 
		 * userService.insertUser(usr);
		 */
		/*
		 * userService.deleteUser(2);
		 * 
		 * List<User> usrs = userService.getAllUsers(); for (User user : usrs) {
		 * System.out.println(user.toString()); }
		 */

		// List<User> usrs = userService.getUserByEmail("%binhtran%");
		// for (User user : usrs) {
		// System.out.println(user.toString());
		// }

		OrdersService ordSvc = new OrdersService();
		List<Orders> ords = ordSvc.getAllOrders();
		Customers customer = null;
		List<OrderDetails> ordDlts = null;
		for (Orders ord : ords) {
			customer = ord.getCustomer();
			ordDlts = ord.getOrdDetails();
			
			// xuat ra customers
			if (customer != null)
				System.out.println(customer.toString());
			
			// xuat ra orders
			System.out.println("|---" + ord.toString());
			
			// xuat ra orderDetails
			if (ordDlts != null) {
				for (OrderDetails ordDlt : ordDlts) {
					System.out.println("    |---" + ordDlt.toString());
				}
			}
			System.out.println("");
			
		}
	}
}
