package com.mappers;

import java.util.List;

import com.domain.Orders;

public interface OrdersMapper {
	public List<Orders> getAllOrders();
}
