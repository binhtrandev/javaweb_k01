package com.services;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.Utils.MyBatisUtil;
import com.domain.Orders;
import com.mappers.OrdersMapper;

public class OrdersService implements OrdersMapper {
	
	@Override
	public List<Orders> getAllOrders() {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory()
				.openSession();
		try {
			OrdersMapper ordersMapper = sqlSession.getMapper(OrdersMapper.class);
			return ordersMapper.getAllOrders();
		} finally {
			sqlSession.close();
		}
	}

}
